﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace app
{
    class Program
    {
        static void Main(string[] args)
        {
            // Fazer um programa que leia traga do banco de dados
            // uma lista dos alunos nas turmas.

            string connectionString = "server=127.0.0.1;user id=root;password=senha;port=3306;database=escola_db;";
            MySqlConnection conn = new MySqlConnection(connectionString);

            conn.Open();

            #region Solução 1
            string query = "select distinct(turma) from alunos order by turma";
            MySqlCommand command = new MySqlCommand(query, conn);
            
            MySqlDataReader reader = command.ExecuteReader();

            List<int> turmas = new List<int>();
            while (reader.Read())
            {
                int turma = (int) reader["turma"];
                turmas.Add(turma);
            }
            reader.Close();


            foreach (var turma in turmas)
            {
                Console.WriteLine("#################################");
                Console.WriteLine(string.Format("Turma: {0}", turma));
                Console.WriteLine();
                Console.WriteLine("Alunos:");

                string query1 = "select nome from alunos join pessoas on alunos.id_pessoa = pessoas.id where turma = {0}";
                MySqlCommand command1 = new MySqlCommand(string.Format(query1, turma), conn);

                MySqlDataReader reader1 = command1.ExecuteReader();

                while (reader1.Read())
                {
                    string nome = (string) reader1["nome"];
                    Console.WriteLine(nome);
                }

                reader1.Close();
            }
            #endregion

            #region Solução 2
            // string query2 = "select nome, turma from alunos join pessoas on alunos.id_pessoa = pessoas.id order by turma";
            // MySqlCommand command2 = new MySqlCommand(query2, conn);

            // MySqlDataReader reader2 = command2.ExecuteReader();

            // int turmaatual = 0;
            // while (reader2.Read())
            // {
            //     int turma = (int) reader2["turma"];
            //     string nome = (string) reader2["nome"];

            //     if (turmaatual != turma)
            //     {
            //         Console.WriteLine("#################################");
            //         Console.WriteLine(string.Format("Turma: {0}", turma));
            //         Console.WriteLine();
            //         Console.WriteLine("Alunos:");
            //         turmaatual = turma;
            //     }
            //     Console.WriteLine(nome);
            // }

            // reader2.Close();
            #endregion

            conn.Close();
        }
    }
}
